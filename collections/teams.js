Teams = new Meteor.Collection('teams');
Contrib = new Meteor.Collection('contrib');

Meteor.methods({
	team: function(teamAttributes) {
		var user = Meteor.user()
			// postWithSameLink = Posts.findOne({url: teamAttributes.url});
		
		// ensure the user is logged in
		if (!user)
			throw new Meteor.Error(401, "You need to login to create a Team");
		
		// ensure the post has a title
		if (!teamAttributes.name)
			throw new Meteor.Error(422, 'Please fill in a headline');
		
		// check that there are no previous posts with the same link
		// if (teamAttributes.url && postWithSameLink) { 
		// 	throw new Meteor.Error(302,	'This link has already been posted', postWithSameLink._id); }
		
		// pick out the whitelisted keys
		var team = _.extend(_.pick(teamAttributes, 'name', 'desc', 'participants'), { 
			userId: user._id,
			author: user.email,
			submitted: new Date().getTime()
		});

		var teamId = Teams.insert(team);
		return teamId; 
	},

	contrib: function(contribAttributes) {
		var user = Meteor.user()
			// postWithSameLink = Posts.findOne({url: contribAttributes.url});
		
		// ensure the user is logged in
		if (!user)
			throw new Meteor.Error(401, "You need to login to create a Team");
		
		// ensure the post has a title
		if (!contribAttributes.title)
			throw new Meteor.Error(422, 'Please fill in a headline');
		
		// check that there are no previous posts with the same link
		// if (contribAttributes.url && postWithSameLink) { 
		// 	throw new Meteor.Error(302,	'This link has already been posted', postWithSameLink._id); }
		
		// pick out the whitelisted keys
		var contrib = _.extend(_.pick(contribAttributes, 'title', 'desc', 'teamId'), { 
			userId: user._id,
			author: user.name,
			submitted: new Date().getTime()
		});

		Contrib.insert(contrib);

	}
});

