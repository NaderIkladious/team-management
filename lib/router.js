Router.configure({ 
	layoutTemplate: 'layout',
	loadingTemplate: 'loading',
    notFoundTemplate: 'notFound',
    waitOn: function() {return [Meteor.subscribe('teams'), Meteor.subscribe('contrib') ] ;}
});
Router.map(function() { 
	this.route('yourTeams', {path: '/'});
	
	this.route('teamPage',{
		path: '/team/:_id',
		data: function() { return Teams.findOne(this.params._id); }
	});

	// this.route('postEdit', {
	// 	path: '/posts/:_id/edit',
	// 	data: function() { return Posts.findOne(this.params._id); }
	// });

	this.route('teamNew', { 
		path: '/new-Team'
	});

	var requireLogin = function() { 
		if (! Meteor.user()) {
			if (Meteor.loggingIn()) 
				this.render(this.loadingTemplate);
			else 
				this.render('accessDenied');

		} else {
			this.next(); 			
		}
	}

	// Router.onBeforeAction('dataNotFound', {only: 'postPage'});
	Router.onBeforeAction(requireLogin, {only: 'teamNew'});
});