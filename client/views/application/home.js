Template.yourTeams.helpers({ 
  teams: function() {
  	var username = Meteor.user().emails[0].address
  		return Teams.find({participants: username}, {sort: {submitted: -1}});    
  },
  leader: function() {
  	var user = Meteor.user()._id;
  		return Teams.find({userId: user}, {sort: {submitted: -1}});
  }
});