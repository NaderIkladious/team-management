Template.teamPage.helpers({ 
  partOf: function() {
    if(Meteor.user()._id === this.userId) {
      return true;
    } else {
      for (var i = this.participants.length - 1; i >= 0; i--) {
        if(this.participants[i] === Meteor.user().emails[0].address) {
          return true;
        }
      };
    }
  	// return Meteor.user().emails[0].address == this.participants;
  },
  contrib: function() {
    return Contrib.find({teamId: this._id}, {sort: {submitted: -1}});
  }
  
});