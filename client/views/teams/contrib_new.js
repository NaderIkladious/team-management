Template.contribNew.events({ 
	'submit form': function(e) {
		e.preventDefault();
		
		var contrib = {
			title: $(e.target).find('[name=title]').val(),
			desc: $(e.target).find('[name=desc]').val(),
			teamId: $('.team-header span').text()
		}
		Meteor.call('contrib', contrib, function(error, id) { 
			if (error)
				return alert(error.reason);
			$(e.target).find('[name=title]').val('');
			$(e.target).find('[name=desc]').val('');
		});
	}
});