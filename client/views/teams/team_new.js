Template.teamNew.events({ 
	'submit form': function(e) {
		e.preventDefault();
		
		var team = {
			name: $(e.target).find('[name=name]').val(),
			desc: $(e.target).find('[name=desc]').val(),
			participants: $(e.target).find('[name=part]').val().split(","),
		}

		Meteor.call('team', team, function(error, id) { 
			if (error)
				return alert(error.reason);

			Router.go('teamPage', {_id: id}); 
		});

		// In your client code: asynchronously send an email
		for (var i = team.participants.length - 1; i >= 0; i--) {
			var currentRec = team.participants[i];
			Meteor.call('sendEmail',
			            currentRec,
			            'info@team-management.com',
			            'Team Management!',
			            'Hello,\nYou were invited by ' + Meteor.user().emails[0].address + 'To join his team `' + team.name + '`, Please Register on the site if you are not a user yet or login to start colaborating.\n\n--\nThanks,\nTeam Management Team.');
			
		};
	}
});